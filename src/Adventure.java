import java.util.*;

public class Adventure {
  Hero spieler;

  public void starten() {
    Scanner sc = new Scanner(System.in);
    // user input
    String inp = "";
    Hero pl = new Hero(100, inp);

    // create start location
    Location s = new Location("Hallo Fremder. Willkommen in unserem bescheidenen Dorf.") {
      @Override
      public void execute() {
        System.out.println("Wie ist dein Name?");
        String inp = sc.next();
        // ...
      }
    };
    // create test location
    Location t1 = new Location("Du bist im Wald.");

    s.addPath(new Path(t1) {
      @Override
      public void execute() {
        if (!pass) {
          // und überprüfe irgendwie, ob alles, was required ist, zutrifft
        }

        pass = true;
      }
    });

    // track current location
    Location c_l = s;
    for (;;) {
      // print sth like "Nach Norden gibt es einen weiteren Weg."

      // we start by printing the desc
      System.out.println(c_l.description);
      // then execute code in the location
      c_l.execute();

      // now waiting for user input
      inp = sc.next();

      // analyze user input
      // TODO: !!! create enum with commands and then a switch
      // TODO: at this position to check what to do next !!!
      if (true) {
        // example for path
        // get path from location
        // TODO: create system to get which path is correct
        Path p = c_l.getPath(inp);
        // execute code in path
        p.execute();
        // pass if allowed to
        if (p.pass) c_l = p.destination;
      }

      // rm later
      break;
    }

    System.out.println(c_l.description);

    // Part here saved in tmp.txt
  }

  public static void main(String[] args) {
    Adventure a = new Adventure();
    a.starten();      
  } 
}
