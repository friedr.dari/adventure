import java.util.ArrayList;

public class Location extends Interactive {
    ArrayList<Path> paths;
    ArrayList<Item> items;
    ArrayList<Entity> entities;
    String description;

    // do we need that?
    public Location(ArrayList<Path> paths, ArrayList<Item> items, ArrayList<Entity> entities, String description) {
        this.paths = paths;
        this.items = items;
        this.entities = entities;
        this.description = description;
    }

    // add for default init
    public Location(String desc) {
        this.paths = new ArrayList<Path>();
        this.items = new ArrayList<Item>();
        this.entities = new ArrayList<Entity>();
        this.description = desc;
    }

    // add possible path to go on
    public void addPath(Path p) {
        // add path
        paths.add(p);
    }

    // convert input to path
    public Path getPath(String inp) {
        return paths.get(0);
    }
}
