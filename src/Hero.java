public class Hero {
  
  // Die Attribute des Helden werden angelegt.
  // Vorgegeben sind hier bereits Leben und Name, weitere Attribute müssen noch hinzugefügt werden
  private int leben;             
  private String name;
  
  
  
  // Hier stehts der Konstruktor der Klasse    
  public Hero(int newLeben, String newName){
    leben = newLeben;
    name = newName;
  }
  
  // Hier beginnen die weiteren Methoden der Klasse
  public void setLeben(int newLeben) {
    if (newLeben >= 0) {
      leben = newLeben;
    } else {
      System.out.println("Fehler: Die Lebenspunkte des Helden können nicht auf einen Wert unter Null gesetzt werden.");
    }
  }
  
  public int getLeben (){
    return leben;  
  }
  
  public void setName (String newName){
    name = newName;
  }
  
  public String getName (){
    return name;
  }
 
  
  
  // Platz für eure eigenen Methoden ...
  
  
}
